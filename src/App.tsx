import React, { useEffect, useState } from "react";
import "./App.css";
import provider from "./Gateways/Provider";
import { createColonyClient } from "./Gateways/ColonyClient";
import EventList, { EventListVm } from "./Components/EventList/EventList";
import {
  ColonyEventName,
  ColonyLog,
  getColonyLogsForEvent,
} from "./Data/EventLogs";
import { getEventListVmFromLogs } from "./Presentation/EventList";

function App() {
  const [eventListVm, setEventListVm] = useState<EventListVm>({
    eventListItemVms: [],
  });

  useEffect(() => {
    async function getLogs() {
      const colonyClient = await createColonyClient(provider);
      const eventsToFetchLogsFor = Object.values(ColonyEventName);
      const eventLogs = await Promise.all(
        eventsToFetchLogsFor.map((eventName) =>
          getColonyLogsForEvent(eventName, colonyClient, provider)
        )
      );
      const flatEventLogs = eventLogs.flat(1);
      const sortedFlatEventLogs: ColonyLog[] = flatEventLogs.sort(
        (a: ColonyLog, b: ColonyLog) =>
          (b.rawLog.blockNumber ?? 0) - (a.rawLog.blockNumber ?? 0)
      );
      setEventListVm(getEventListVmFromLogs(sortedFlatEventLogs));
    }
    getLogs();
  }, []);
  return (
    <div className="App">
      <EventList vm={eventListVm} />
    </div>
  );
}

export default App;
