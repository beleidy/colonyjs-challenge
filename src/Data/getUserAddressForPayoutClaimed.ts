import { ColonyClient } from "@colony/colony-js";
import { utils } from "ethers";
import { LogDescription } from "ethers/utils";

export async function getUserAddressForPayoutClaimedParsedEventLog(
  payoutClaimedParsedEventLog: LogDescription,
  colonyClient: ColonyClient
) {
  const humanReadableFundingPotId = new utils.BigNumber(
    payoutClaimedParsedEventLog.values.fundingPotId
  ).toString();

  const { associatedTypeId } = await colonyClient.getFundingPot(
    humanReadableFundingPotId
  );

  const { recipient: userAddress } = await colonyClient.getPayment(
    associatedTypeId
  );

  return userAddress;
}
