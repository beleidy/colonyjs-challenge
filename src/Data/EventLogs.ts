import { ColonyClientV4, getLogs } from "@colony/colony-js";
import { EventFilter } from "ethers";
import { InfuraProvider, Log } from "ethers/providers";
import { LogDescription } from "ethers/utils";
import { getBlockTimeFromBlockHash } from "./getBlockTimeFromBlockHash";
import { getUserAddressForPayoutClaimedParsedEventLog } from "./getUserAddressForPayoutClaimed";

export type ColonyLog = {
  rawLog: Log;
  parsedLog: LogDescription;
  logTime: number | null;
  userAddress: string;
};

export enum ColonyEventName {
  ColonyInitialised = "ColonyInitialised",
  ColonyRoleSet = "ColonyRoleSet",
  PayoutClaimed = "PayoutClaimed",
  DomainAdded = "DomainAdded",
}

export async function getColonyLogsForEvent(
  eventName: ColonyEventName,
  colonyClient: ColonyClientV4,
  provider: InfuraProvider
): Promise<ColonyLog[]> {
  // After discussion with Raul, no arguments were added for now
  // It doesn't feel right, happy to discuss my thoughts if of interest
  // @ts-expect-error
  const eventFilter: EventFilter = colonyClient.filters[eventName]();

  const rawLogs = await getLogs(colonyClient, eventFilter);

  const logs: ColonyLog[] = await Promise.all(
    rawLogs.map(async (rawLog) => {
      const parsedLog = colonyClient.interface.parseLog(rawLog);
      let userAddress: string = "";
      if (parsedLog.name === ColonyEventName.PayoutClaimed) {
        userAddress = await getUserAddressForPayoutClaimedParsedEventLog(
          parsedLog,
          colonyClient
        );
      }
      return {
        rawLog,
        parsedLog,
        userAddress,
        logTime: rawLog.blockHash
          ? await getBlockTimeFromBlockHash(provider, rawLog.blockHash)
          : null,
      };
    })
  );

  return logs;
}
