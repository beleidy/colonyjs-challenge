import { getBlockTime } from "@colony/colony-js";
import { InfuraProvider } from "ethers/providers";

export async function getBlockTimeFromBlockHash(
  provider: InfuraProvider,
  blockHash: string
) {
  // Use the blockHash to look up the actual time of the block that mined the transactions of the current event
  const logTime = await getBlockTime(provider, blockHash);

  return logTime;
}
