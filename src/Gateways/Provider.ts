import { InfuraProvider } from "ethers/providers";

// Get a new Infura provider (don't worry too much about this)
const provider = new InfuraProvider();

export default provider;
