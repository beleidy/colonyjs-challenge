import { EventListItemVm } from "../Components/EventList/EventListItem/EventListItem";
import { ColonyLog } from "../Data/EventLogs";
import { convertBigNumberHexToString } from "./utils/convertBigNumberHexToString";
import { formatDate } from "./utils/DateFormatter";

export function getPayoutClaimedEventListItemVmFromLog(
  log: ColonyLog
): EventListItemVm {
  const tokenMap: { [k: string]: string } = {
    "0x6B175474E89094C44Da98b954EedeAC495271d0F": "DAI",
    "0x0dd7b8f3d1fa88FAbAa8a04A0c7B52FC35D4312c": "BLNY",
  };

  return {
    key: `${Math.random()}`,
    primaryTextVm: {
      primaryTextItems: [
        {
          text: "User ",
          isHeavy: false,
        },
        {
          text: `${log.userAddress}`,
          isHeavy: true,
        },
        {
          text: " claimed ",
          isHeavy: false,
        },
        {
          text: `${convertBigNumberHexToString(log.parsedLog.values.amount)}`,
          isHeavy: true,
        },
        {
          text: `${
            tokenMap[log.parsedLog.values.token as string] ??
            log.parsedLog.values.token
          }`,
          isHeavy: true,
        },
        {
          text: " payout from pot ",
          isHeavy: false,
        },
        {
          text: `${log.parsedLog.values.fundingPotId}`,
          isHeavy: true,
        },
        {
          text: ".",
          isHeavy: false,
        },
      ],
    },
    secondaryText: log.logTime ? formatDate(new Date(log.logTime)) : "No date",
    avatarVm: {
      seed: log.userAddress,
    },
  };
}
