import { EventListItemVm } from "../Components/EventList/EventListItem/EventListItem";
import { ColonyLog } from "../Data/EventLogs";
import { ColonyRole } from "@colony/colony-js";
import { formatDate } from "./utils/DateFormatter";

export function getColonyRoleSetEventListItemVmFromLog(
  log: ColonyLog
): EventListItemVm {
  return {
    key: `${Math.random()}`,
    primaryTextVm: {
      primaryTextItems: [
        {
          text: `${ColonyRole[log.parsedLog.values.role]}`,
          isHeavy: true,
        },
        {
          text: " role assigned to user ",
          isHeavy: false,
        },
        {
          text: `${log.parsedLog.values.user}`,
          isHeavy: true,
        },
        {
          text: " in domain ",
          isHeavy: false,
        },
        {
          text: `${log.parsedLog.values.domainId}`,
          isHeavy: true,
        },
        {
          text: ".",
          isHeavy: false,
        },
      ],
    },
    secondaryText: log.logTime ? formatDate(new Date(log.logTime)) : "No date",
    avatarVm: {
      seed: log.parsedLog.values.user,
    },
  };
}
