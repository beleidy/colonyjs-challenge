import { utils } from "ethers";

export function convertBigNumberHexToString(bigNumberHex: string): string {
  // Create a new BigNumber instance from the hex string amount in the parsed log
  const humanReadableAmount = new utils.BigNumber(bigNumberHex);

  // Get a base 10 value as a BigNumber instance
  const wei = new utils.BigNumber(10);

  // The converted amount is the human readable amount divided by the wei value raised to the power of 18
  const convertedAmount = humanReadableAmount.div(wei.pow(18));

  // If you are confident that it's a low enough value, you can display it as an integer -- .toNumber()
  // But to be on the safe side, you can also use it as a string
  return convertedAmount.toString();
}
