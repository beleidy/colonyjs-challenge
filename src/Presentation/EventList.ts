import { EventListVm } from "../Components/EventList/EventList";
import { EventListItemVm } from "../Components/EventList/EventListItem/EventListItem";
import { ColonyEventName, ColonyLog } from "../Data/EventLogs";
import { getColonyInitialisedEventListItemVmFromLog } from "./ColonyInitialised";
import { getColonyRoleSetEventListItemVmFromLog } from "./ColonyRoleSet";
import { getDomainAddedEventListItemVmFromLog } from "./DomainAdded";
import { getPayoutClaimedEventListItemVmFromLog } from "./PayoutClaimed";

export function getEventListVmFromLogs(logs: ColonyLog[]): EventListVm {
  const eventNameVmCreatorMap: {
    [k in ColonyEventName]: (...args: any) => EventListItemVm;
  } = {
    [ColonyEventName.ColonyInitialised]:
      getColonyInitialisedEventListItemVmFromLog,
    [ColonyEventName.ColonyRoleSet]: getColonyRoleSetEventListItemVmFromLog,
    [ColonyEventName.DomainAdded]: getDomainAddedEventListItemVmFromLog,
    [ColonyEventName.PayoutClaimed]: getPayoutClaimedEventListItemVmFromLog,
  };

  const eventListItemVms = logs
    .filter((log: ColonyLog) => log.parsedLog.name in ColonyEventName)
    .map((log: ColonyLog) =>
      eventNameVmCreatorMap[log.parsedLog.name as ColonyEventName](log)
    );

  return {
    eventListItemVms,
  };
}
