import { EventListItemVm } from "../Components/EventList/EventListItem/EventListItem";
import { ColonyLog } from "../Data/EventLogs";
import { formatDate } from "./utils/DateFormatter";

export function getDomainAddedEventListItemVmFromLog(
  log: ColonyLog
): EventListItemVm {
  return {
    key: `${Math.random()}`,
    primaryTextVm: {
      primaryTextItems: [
        {
          text: "Domain ",
          isHeavy: false,
        },
        {
          text: `${log.parsedLog.values.domainId}`,
          isHeavy: true,
        },
        {
          text: " added.",
          isHeavy: false,
        },
      ],
    },

    secondaryText: log.logTime ? formatDate(new Date(log.logTime)) : "No date",
    avatarVm: {
      seed: `${log.parsedLog.values.domainId}`,
    },
  };
}
