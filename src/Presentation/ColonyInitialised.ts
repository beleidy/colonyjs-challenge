import { EventListItemVm } from "../Components/EventList/EventListItem/EventListItem";
import { ColonyLog } from "../Data/EventLogs";
import { formatDate } from "./utils/DateFormatter";

export function getColonyInitialisedEventListItemVmFromLog(
  log: ColonyLog
): EventListItemVm {
  return {
    key: `${Math.random()}`,
    primaryTextVm: {
      primaryTextItems: [
        {
          text: "Congratulations! It's a beautiful baby colony!",
          isHeavy: false,
        },
      ],
    },
    secondaryText: log.logTime ? formatDate(new Date(log.logTime)) : "No date",
    avatarVm: {
      seed: `${log.parsedLog.values.colonyNetwork}`,
    },
  };
}
