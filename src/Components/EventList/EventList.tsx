import React, { VoidFunctionComponent } from "react";
import EventListItem, { EventListItemVm } from "./EventListItem/EventListItem";
import styles from "./EventList.module.css";

export interface EventListVm {
  eventListItemVms: EventListItemVm[];
}

const EventList: VoidFunctionComponent<{ vm: EventListVm }> = ({ vm }) => {
  return (
    <div className={styles.eventList}>
      {vm.eventListItemVms.map((eventListItemVm) => (
        <EventListItem key={eventListItemVm.key} vm={eventListItemVm} />
      ))}
    </div>
  );
};

export default EventList;
