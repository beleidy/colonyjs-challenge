import React, { VoidFunctionComponent } from "react";
import Avatar, { AvatarVm } from "../../Avatar/Avatar";
import styles from "./EventListItem.module.css";

export interface PrimaryTextItem {
  text: string;
  isHeavy: boolean;
}

export interface PrimaryTextVm {
  primaryTextItems: PrimaryTextItem[];
}

export interface EventListItemVm {
  key: string;
  primaryTextVm: PrimaryTextVm;
  secondaryText: string;
  avatarVm: AvatarVm;
}

const PrimaryText: VoidFunctionComponent<{ vm: PrimaryTextVm }> = ({ vm }) => {
  return (
    <>
      {vm.primaryTextItems.map((textItem) => (
        <span
          key={textItem.text}
          className={textItem.isHeavy ? styles.heavy : undefined}
        >
          {textItem.text}
        </span>
      ))}
    </>
  );
};

const EventListItem: VoidFunctionComponent<{ vm: EventListItemVm }> = ({
  vm,
}) => {
  return (
    <div className={styles.eventListItem}>
      <Avatar vm={vm.avatarVm} />
      <div className={styles.copy}>
        <div className={styles.primaryCopy}>
          <PrimaryText vm={vm.primaryTextVm} />
        </div>
        <div className={styles.secondaryCopy}>{vm.secondaryText}</div>
      </div>
    </div>
  );
};

export default EventListItem;
