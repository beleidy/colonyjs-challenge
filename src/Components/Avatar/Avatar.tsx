import { VoidFunctionComponent } from "react";
import Blockies from "react-blockies";
import styles from "./Avatar.module.css";

export interface AvatarVm {
  seed: string;
}

const Avatar: VoidFunctionComponent<{ vm: AvatarVm }> = ({ vm }) => {
  return (
    <div className={styles.avatar}>
      <Blockies seed={vm.seed} size={12} />
    </div>
  );
};

export default Avatar;
